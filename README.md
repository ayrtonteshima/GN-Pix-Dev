# API Pix | GerenciaNet

Criei um código base onde estabeleço uma conexão com a API (credenciais, token..) e crio uma lib em cima dos endpoints.
Basicamente a única coisa que faz (por enquanto) é gerar o QR Code com base em uma cobrança nova toda vez que acessar a rota base `/`.

Os outros dois endpoints listados em `src/server.js` são basicamente para demonstrar o fácil uso da lib que criei.

A ideia (que devo deixar para um próximo vídeo) era demonstrar o uso em um pequeno projeto mais elaborado, porém, desenvolvendo percebir que o vídeo ficaria absurdamente longo.

Então resolvi no primeiro vídeo fazer todas as explicações de fluxo (de forma visual) e também prático (apenas demonstrando a autenticação, geração de QR code, criação da lib e implementação do endpoint do webhook).

## Dependências
- Node v15.5.0

## Setup
Crie uma pasta na raíz chamado `certs` e adicione o certificado lá.
O nome do certificado deve estar na variável de ambiente `GN_CERT`

## Rodando
```
npm start
```

## Variáveis de ambiente
Renomear o arquivo `.env-sample` localizado na raíz do projeto para `.env` e preencher os valores lá especificados.

## Arquivo notion
https://www.notion.so/Roteiro-b394e157f9e14527a0388d46b5881b80