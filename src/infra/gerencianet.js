const axios = require("axios");
const https = require("https");
const fs = require("fs");
const path = require("path");

const cert = fs.readFileSync(
  path.resolve(__dirname, `../../certs/${process.env.GN_CERT}`)
);

const agent = new https.Agent({
  pfx: cert,
  passphrase: '',
});

const authenticate = ({ clientID, clientSecret }) => {
  const dataCredentials = `${clientID}:${clientSecret}`;

  const auth = Buffer.from(dataCredentials).toString('base64');

  const options = {
    url: `${process.env.GN_ENDPOINT}/oauth/token`,
    method: 'POST',
    headers: {
      Authorization: `Basic ${auth}`,
      'Content-Type': 'application/json',
    },
    httpsAgent: agent,
    data: { grant_type: 'client_credentials' }

  };

  return axios(options);
};

const GerenciaNetConnection = async ({ clientID, clientSecret }) => {
  try {
    const response = await authenticate({ clientID, clientSecret });
    const accessToken = response.data?.access_token;
    const configs = {
      baseURL: `${process.env.GN_ENDPOINT}`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        'Content-Type': 'application/json',
      },
      httpsAgent: agent,
    };

    return axios.create(configs);
  } catch(e) {
    console.error(e.message);
    throw new Error('Autenticação com API GerenciaNet');
  }
};

module.exports = GerenciaNetConnection;