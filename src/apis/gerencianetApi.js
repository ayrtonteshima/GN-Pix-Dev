const url = require('url');
const GerenciaNetConnection = require('../infra/gerencianet');

let GNConnection = null;

const setConnection = async ({ clientID, clientSecret }) => {
  if (!GNConnection) {
    GNConnection = await GerenciaNetConnection({
      clientID,
      clientSecret,
    });
  }
  return GNConnection;
};

const lib = {
  cob: {
    find(txtId) {
      return this.GNApi.post(`/v2/cob/${txtId}`);
    },
    list({ inicio, fim }) {
      const query = new url.URLSearchParams({ inicio, fim });
      return this.GNApi.get(`/v2/cob?${query.toString()}`)
    },
    create(data) {
      return this.GNApi.post(`/v2/cob`, data);
    },
    qrCode(locationId) {
      return this.GNApi.get(`/v2/loc/${locationId}/qrcode`)
    }
  },
  pix: {
    list({ inicio, fim }) {
      const query = new url.URLSearchParams({ inicio, fim });
      return this.GNApi.get(`/v2/pix?${query.toString()}`)
    },
    find(e2eid) {
      return this.GNApi.get(`/v2/pix/${e2eid}`) 
    }
  },
  webhook: {
    register(chave) {
      return this.GNApi.put(`/v2/webhook/${chave}`);
    }
  }
};

const makeLibProxy = (GNApiAlready, lib) => new Proxy(lib, {
  get(target, prop) {
    return async (...args) => {
      // Oportunidade do refresh token
      // Oportunidade para pegar do cache
      const GNApi = await GNApiAlready;
      return Reflect.get(target, prop).call({ GNApi }, ...args);
    }
  }
})

module.exports = ({ clientID, clientSecret }) => {
  const GNApi = setConnection({ clientID, clientSecret });

  return Object.fromEntries(
      Object.entries(lib)
        .map(([resource, methods]) => [resource, makeLibProxy(GNApi, methods)])
      )
};
