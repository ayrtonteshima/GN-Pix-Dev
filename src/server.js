require('dotenv').config();

const express = require('express')
const GerenciaNet = require('./apis/gerencianetApi');

const PORT = process.env.PORT || 3002;

const gnApi = GerenciaNet({
  clientID: process.env.GN_CLIENT_ID,
  clientSecret: process.env.GN_CLIENT_SECRET
});

const app = express();

app.set('view engine', 'ejs');
app.set('views', 'src/views');

// error: invalid_token { data: { error: 'invalid_token' } }

app.get('/', async (req, res) => {
  const { data: cob } = await gnApi.cob.create({ 
    calendario: {
      expiracao: 3600
    },
    valor: {
      original: '0.10'
    },
    chave: '126bec4a-2eb6-4b79-a045-78db68412899'
   });

   const { data: qrCode } = await gnApi.cob.qrCode(cob.loc.id);
  res.render('qrcode', { qrcodeImage: qrCode.imagemQrcode });
});

app.get('/pix', async (req, res) => {
  const { data: pix } = await gnApi.pix.list({ inicio: '2021-02-01T00:00:00Z', fim: '2021-02-08T23:59:00Z' });

  res.json(pix);
});

app.get('/pix/:e2eid', async (req, res) => {
  const { data: pix } = await gnApi.pix.find(req.params.e2eid);
  res.json(pix);
})

app.listen(PORT, () => {
  console.log('running')
});